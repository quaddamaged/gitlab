export const TEMPLATES = [
  'Project #1',
  [
    { name: 'Bug', project_id: 1 },
    { name: 'Documentation', project_id: 1 },
    { name: 'Security release', project_id: 1 },
  ],
];

export const MOCK_CUSTOM_EMAIL_EMPTY = {
  custom_email: null,
  custom_email_enabled: false,
  custom_email_verification_state: null,
  custom_email_verification_error: null,
  custom_email_smtp_address: null,
  error_message: null,
};
